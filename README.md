pi-rc
=====

Drive a radio controlled car using your Raspberry Pi! See [Turn your Raspberry
Pi into a radio controller for RC
vehicles](http://brskari.wordpress.com/2014/06/02/turn-your-raspberry-pi-into-a-radio-controller-for-rc-vehicles/)
